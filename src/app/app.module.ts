import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AryService} from './ary.service';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [AryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
