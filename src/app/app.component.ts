import { Component } from '@angular/core';
import {AryService} from './ary.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private aryService: AryService) {}
  public arys: number[][] = [
    this.aryService.ary(10),
    this.aryService.ary(10)
  ];

  public regen () {
    this.arys = [
      this.aryService.ary(10),
      this.aryService.ary(10)
    ];
  }
}
